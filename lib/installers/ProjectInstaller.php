<?php
    namespace Zimplify\Scaffold\Application\Installer;

    /**
     * the Project Installer deploys all needed structure after the create-project command is called from composer
     * - generate the soft links
     * - deploy needed directories
     * 
     * @package Zimplify (10)
     * @subpackage Scaffold (01)
     * @internal Application (01)
     * @category Installer (05)
     * @api: ProjectInstaller (01)
     */    
    final class ProjectInstaller {

        const DEF_ROOT_LEVEL = "./";
        const DEF_BIND_FILE = "./lib/installers/links.json";
        const DEF_COMPOSER_NEW = "./lib/Application.json";
        const DEF_COMPOSER = "./composer.json";
        const DIR_APP = ["controller", "interfaces", "messages", "middlewares", "models", "plugins", "traits"];
        const DIR_ASSETS = ["optimize/extensions", "raw", "optimize/rules"];
        const DIR_ROOT = ["app", "assets", "config", "lib", "test"];
        const FILE_INDEX = "./index.php";
        const FILE_HTACCESS = "./.htacess";
        const LN_INDEX = "./lib/slim/zimplify-start.php";
        const LN_HTACCESS = "./lib/slim/rewrite.apache";

        /**
         * creating the necessary links, subdirectories that is removed during git.
         * @return void
         */
        public static function install() : void {

            // adding directories
            static::deploy();

            // now link up the files
            if (file_exists(self::DEF_BIND_FILE)) 
                foreach (json_decode(file_get_contents(self::DEF_BIND_FILE), true) as $t => $s) 
                    static::link($s, $t);

            // finally deploy the application composer file
            if (file_exists(self::DEF_COMPOSER_NEW)) rename(self::DEF_COMPOSER_NEW, self::DEF_COMPOSER);

        }

        /**
         * generating directories needed for solution
         * @return void
         */
        private static function deploy() : void {
            foreach (self::DIR_ROOT as $d)
                if (!file_exists(($r = self::DEF_ROOT_LEVEL.$d))) 
                    static::make($r);
                else 
                    if (defined(get_class()."::DIR_".strtoupper($d))) 
                        foreach (constant(get_class()."::DIR_".strtoupper($d)) as $sd) 
                            static::make($r."/".$sd);
        }

        /**
         * creating a new direction based on $name
         * @param string $name the name of the directory to create
         */
        private static function make(string $name) : void {
            mkdir($name, 0755, true);
        }

        /**
         * define all required symbolic link for application
         * @param string $source the source file to provide data
         * @param string $target the alias name for the $source
         * @return void
         */
        public static function link(string $source, string $target) : void {
            if (!file_exists($target)) 
                symlink($source, $target);
        }
    }