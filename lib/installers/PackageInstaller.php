<?php
    namespace Zimplify\Scaffold\Application\Installers;
    use Composer\Installer\PackageEvent;
    use Zimplify\Zimplier\Application;    
    use ProjectInstaller;

    /**
     * the Package Installer is called whenever composer do a require command - this should be called AFTER install the package
     * - add to scripts -> post-package-install
     * - with: "post-package-install": "Zimplify\\Scaffold\\Application\\Installers\\PackageInstaller::install"
     * 
     * @package Zimplify (10)
     * @subpackage Scaffold (01)
     * @internal Application (01)
     * @category Installer (05)
     * @api: PackageInstaller (02)
     */    
    final class PackageInstaller {

        const ERR_BAD_INSTALL = 1001010502001;
        const ERR_BAD_NAME = 1001010502002;
        const ERR_BAD_PACKAGE = 1001010502004;
        const ERR_UNKNOWN_COMMAND = 1001010502003;
        const EXT_CONFIG_FILE = ".json";
        const INST_ACTIVITIES = "activities";
        const INST_ASSETS = "assets";
        const INST_NAME = "name";
        const INST_VERSION = "version";
        const INST_SETTINGS = "setting";
        const INST_MAPPING = "plugins";
        const PATH_OF_ACTIVITES = "./config/activities.json";
        const PATH_OF_CONFIG = "./config/services/";
        const PATH_OF_INSTALLATIONS = "./config/packages.json";
        const PATH_OF_PACKAGE = "install/package.json";
        const PATH_OF_PLUGINS = "./config/plugins.json";
        const PATH_TO_ASSETS = "./assets";
        const PATH_TO_REBUILD =  "./.rebuild";

        /**
         * configuring the settings of the package
         * @param string $package the name of the package to call
         * @param string $ettings the name of the setting file
         */
        protected static function configure(string $package, string $settings) : void {
            if (file_exists($f = self::PATH_OF_CONFIG.$package.self::EXT_CONFIG_FILE)) {
                $d = static::update(json_decode(file_get_contents($f), true), json_decode(file_get_contents($settings), true));                
                file_put_contents(self::PATH_OF_CONFIG, json_encode($d));
            } else 
                ProjectInstaller::link($settings, $f);   
        }

        /**
         * this is the main function we call when after installing a package
         * @return void
         */
        public static function install(PackageEvent $event) : void {
            
            // loading the composer event data
            $m = $event->getComposer()->getInstallationManager();
            $p = $event->getOperation()->getPackage();

            // getting our path information and names
            $d = $m->getInstallPath($p);
            $n = $p->getName();            

            // check if the package exists
            if (file_exists($s = $d."/".self::PATH_OF_PACKAGE)) {
                $y = "";
                foreach (json_decode(file_get_contents($s), true) as $i => $a) {
                    switch ($i) {

                        // if we got the package name
                        case self::INST_NAME: 
                            $y = $a; 
                            break;

                        // if we got activities
                        case self::INST_ACTIVITIES:
                            static::prepare($a);
                            break;

                        // if we got the list of assets
                        case self::INST_ASSETS:
                            static::transfer(self::PATH_TO_ASSETS, $a);
                            break;
                        
                        // if we got plugin mapping
                        case self::INST_MAPPING:
                            static::map($a);
                            break;

                        // if we received a new setting file
                        case self::INST_SETTINGS:
                            if ($y)
                                static::configure($y, $d.$a);
                            else 
                                throw new \UnexpectedValueException("Failed to configure the package.", self::ERR_BAD_NAME);
                            break;                            
                        
                        // if we got the version data - write to packages
                        case self::INST_VERSION:
                            if ($y && file_exists(self::PATH_OF_INSTALLATIONS)) {
                                $f = json_decode(file_get_contents(self::PATH_OF_INSTALLATIONS), true)[$n] = $a;
                                file_put_contents(self::PATH_OF_INSTALLATIONS, json_encode($f));
                            } else                                 
                                throw new \RuntimeException("Unable to locate installation file.", self::ERR_BAD_INSTALL);
                            break;

                        // if others, reject the request
                        default:
                            throw new \UnexpectedValueException("Unknown directive $i.", self::ERR_UNKNOWN_COMMAND);
                    }
                }
            }

            // finally force the system to rebuild zimplify.json
            file_put_contents(self::PATH_TO_REBUILD, "");
        }

        /**
         * check the current mappings and add package data to the map
         * @param string $source the pacakge's mapping file
         * @return void
         */
        protected static function map(string $source) : void {
            if (file_exists(self::PATH_OF_PLUGINS)) {
                $o = json_decode(file_get_contents(self::PATH_OF_PLUGINS), true);
                
                foreach (json_decode(file_get_contents($source), true) as $p => $c) 
                    if (array_key_exists($p, $o)) 
                        $o[$p] = $c;
                
                file_put_contents(self::PATH_OF_PLUGINS, json_encode($o));
            } else 
                throw new \RuntimeException("Cannot find plugin definition file.", self::ERR_BAD_INSTALL);
        }

        /**
         * check our activities and provide mapping if not defined,
         * @param string $source the source file from incoming package
         * @return void
         */
        protected static function prepare(string $source) : void {
            if (file_exists($source)) {
                if (file_exists(self::PATH_OF_ACTIVITES)) {
                    $o = json_decode(file_get_contents(self::PATH_OF_ACTIVITES), true);
                    $n = json_decode(file_get_contents($source), true);
                    file_put_contents(self::PATH_OF_ACTIVITES, static::update($o, $n));
                } else 
                    throw new \RuntimeException("Expected application activities manifest but none found.", self::ERR_BAD_INSTALL);
            } else  
                throw new \RuntimeException("Expected activities manifest none given.", self::ERR_BAD_PACKAGE);
        }

        /**
         * transferring the assets from the package to application
         * @param string $target the target directory to link the data
         * @param string $source the source directory
         * @param string $parent (optional) the subtype of file to store
         * @return void
         */
        protected static function transfer(string $target, string $source, string $parent = null) : void {
            if (file_exists($path) && is_dir($path)) {
                $d = dir($path);

                // we are reading the path inside
                while (($i = $d->read()) !== false) {
                    if ($i !== "." && $i !== "..") {
                        $s = $path."/".$i;
                        if (is_dir($s)) 
                            static::transfer($s, $i);
                        else {
                            $t = $target."/".($parent ? $parent."/" : "").$i;
                            ProjectInstaller::link($s, $t);
                        }
                    }
                }
            } else 
                throw new \RuntimeException("Project assets are not available.", self::ERR_BAD_PACKAGE);
        }

        /**
         * comparing the data between $source and $target and insert new entries if not presented
         * @param array $source the original arrya
         * @param array $target the incoming target data
         * @return array the combined data
         */
        protected static function update(array $source, array $target) : array {
            foreach ($target as $f => $v) 
                if (!array_key_exists($f, $source)) 
                    $source[$f] = $v;
                else
                    if (is_array ($source[$f])) 
                        $source[$f] = static::update($source[$f], $v);
            return $source;
        }
    }