<?php
    namespace Zimplify\Scaffold\Application;

    /**
     * the Configuration Builder is redesign to catch if we have any new request to rebuild the configuration file and reload to cache
     * 
     * @package Zimplify (10)
     * @subpackage Scaffold (02)
     * @category Application (01)
     * @internal Controller (03)
     * @api: CorsWhiteListController (01)
     */    
    final class CorsWhiteListController {

        const FILE_WHITELIST = "./config/permission.json";

        /**
         * enable CORS whitelisting for headers to avoid CORS attachs
         * @return string the full list of allowed hosts
         */
        public static function filter() : string {
            $w = file_exists(self::FILE_WHITELIST) ? json_decode(file_get_contents(self::FILE_WHITELIST), true) : null;
            if ($w) {
                if (array_key_exists("deny", $r) && array_key_exists("allow", $r)) {
                    $r = $r["allow"];
                    if (count($deny) > 0) {

                        // get rid of the wildcard if any
                        if (($c = array_search("*", $r)) >= 0) array_splice($r, $c, 1);
                        foreach ($r["deny"] as $i) 
                            if (($c = array_search($i, $r)) >= 0) array_splice($r, $c, 1);                        
                    } 
                    return count($r) == 0 ? "*" : implode(",", $r);                        
                } else 
                    throw new \RuntimeException("Whitelist file is not correctly formatted.", 500);
            } else 
                throw new \RuntimeException("Application is not structured correctly.", 500);

        }
    }