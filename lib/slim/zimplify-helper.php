<?php
    /**
     * Zimplify Scaffold Helper
     * 
     * This file contains some required function for the scaffold to start properly.
     */
    use Zimplify\Zimplier\Support\CacheOperator as cache;
    use Zimplify\Zimplier\Application as app;
    use Slim\Http\Request as request;

    define ('CFG_CORE_DEBUG', 'services.core.debug');
    define ("DEF_CFG_ROUTING", "routing");
    define ("DEF_DIR_CLASSES", "./app/controllers"); 

    /**
     * compiling the core configuration based on all snipplets
     * @param string $path the path where configurations are stored
     * @return string the JSONed structure of the environment data
     */
    function compile(string $path) : string {
        $result = [];
        $d = dir($path);
        while (false !== ($e =$d->read()))
            if ($e != "." && $e != "..") {
                $n = explode("/", $path."/".$e);
                $n = explode(".", end($n))[0];
                $result[$n] = json_decode((is_dir($path."/".$e) ? compile($path."/".$e) : file_get_contents($path."/".$e)), true);
            }
        return json_encode($result);
    }

    /**
     * packaging the data received into something we can send back
     * @param Slim\Http\Request Slim request we received.
     * @param mixed $result the source data to return
     * @return array the result array to send back
     */
    function package(request $req, $output) : array {
        $result = [];
        if (app::env(CFG_CORE_DEBUG)) {
            $input = [];
            $input["command"] = $req->getMethod();
            $input["endpoint"] = $req->getPath();
            $input["body"] = $req->getParsedBody();
            $result["input"] = $input;
        }
        $result["output"] = $output;
        return $result;
    }

    /**
     * loading classes into references of the PHP repository
     * @param string $path the source path that holds the files to include
     * @return void
     */
    function reference(string $path) {
        $d = dir($path);
        while (false !== ($e =$d->read())) 
            if ($e != "." && $e != "..")                 
                if (is_dir($path."/".$e)) {
                    reference($path."/".$e);
                } else 
                    require_once $path."/".$e;
    }

    /**
     * staging configuration to be uploaded onto cache
     * @param string $filename the file to load
     * @param bool $override (optional) whether the data should be overriden if already cached
     * @return bool whether was successfully cache
     */
    function stage(string $filename, bool $unpack = false, bool $override = false) {
        $result = false;
        if (file_exists($filename)) {
            $f = explode("/", $filename);
            $n = explode(".", end($f)); array_unshift($n, array_pop($n));
            $t = implode(".", $n);
            $d = file_get_contents($filename);                        
            if (!cache::exists($t) || (cache::exists($t) && $override)) 
                $result = cache::set($t, $unpack ? json_decode($d, true) : $d);
        }
        return $result;
    }

    /**
     * compiling a list of file into any array and output as json 
     * @param string $directory the source directory
     * @return string the JSON structure
     */
    function transform(string $directory) : string {
        $h = is_dir($directory) ? dir($directory) : null;
        $r = ["server" => app::env("services.core.zesbri.server")];
        $c = [];

        // looping through the directory...
        while (($f = $h ? $h->read() : false) !== false ) {
            if ($f != "." || $f != "..") {
                $d = $directory."/".$f;
                if (!is_dir($d) && file_exists($d)) array_push($c, json_decode(file_get_contents($d), true));                
            }
        }
        $r["commands"] = $c;

        // return the result
        return json_encode($r);
    }    

    /**
     * matching the routing configuration against HOST and determine what routes to take for the request
     * @retrun string the routing base
     */
    function where() : string {
        // we use HTTP REERER here to solve the dilemma with virtual host server connecting to Zimplify application.
        $h = $_SERVER["HTTP_REFERER"] ?? $_SERVER["SERVER_NAME"];
        if (array_key_exists($h, $r = app::env(DEF_CFG_ROUTING))) {
            app::arm("datasource", ["value" => (array_key_exists("datasource", $r) ? $r["datasource"] : "default")]);
            return $r[$h]["route"];
        } else 
        throw new \InvalidArgumentException("Unable to find matching option", 404);
    }