<?php
    /**
     * this is the starting file for the Icing project and to initiate all routers with Slim 3.X framework
     * it will load a series of support files and those are located in the /src directory.
     * we will also load the routes from the user source which is located in /usr/router.php
     * 
     * once it is loaded the framework will start
     */

    // default auto load to PSR
    require "./vendor/autoload.php"; 
    require_once "./core/slim/helper.php";

    // some key constants here
    define("SLIM_SETTINGS", "./lib/slim/slim-settings.php");
    define("SLIM_DEPENDENCIES", "./lib/slim/slim-dependencies.php");
    define("SLIM_APP_ROUTES", "./app/routes/default.route");

    // sourcing Slim Framework library
    use Slim\App as app;    
    use Zimplify\Scaffold\Application\Controllers\CorsWhiteListController as cors;

    // handling CORS request and what gets returned
    header('Access-Control-Allow-Origin:'.cors::filter());
    header('Access-Control-Allow-Headers: Content-Type, X-Custom-Token');
    header('Access-Control-Allow-Methods: GET, PUT, POST, PATCH, DELETE, OPTIONS');
    
    // loading the application for slim
    $app = new app(require_once SLIM_SETTINGS);
    require_once SLIM_DEPENDENCIES;
    require_once SLIM_APP_ROUTES;
    $app->run();
    
        
