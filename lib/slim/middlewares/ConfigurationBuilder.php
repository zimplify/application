<?php
    namespace Zimplify\Scaffold\Application\Middlewares;
    use Zimplify\Common\ArrayUtils;
    use Zimplify\Common\CacheUtils;
    use Zimplify\Scaffold\Application\Installers\PackageInstaller;
    use Slim\Http\Request as request;
    use Slim\Http\Response as response;
    
    /**
     * the Configuration Builder is redesign to catch if we have any new request to rebuild the configuration file and reload to cache
     * 
     * @package Zimplify (10)
     * @subpackage Scaffold (02)
     * @category Application (01)
     * @internal Middleware (02)
     * @api: ConfigurationBuilder (02)
     */
    class ConfigurationBuilder {

        const CACHE_CONFIG = "json.settings";
        const DLT_DOT = ".";
        const FILE_REBUILD = "./.rebuild";
        const PATH_TO_CONFIG = "./config";
        const PATH_OF_CONFIG = "zimplify.conf";

        /**
         * this is our real procedure
         * @param  ServerRequestInterface $request  PSR7 request
         * @param  ResponseInterface      $response PSR7 response
         * @param  callable               $next     Next middleware
         * @return \Psr\Http\Message\ResponseInterface
         */
        public function __invoke(request $req, response $res, callable $next) {
            try {
                if (file_exists(self::FILE_REBUILD)) 
                    if ($d = $this->build(self::PATH_TO_CONFIG, self::PATH_OF_CONFIG)) 
                        CacheUtils::set(self::CACHE_CONFIG, $d);
                else  
                    if (is_null(CacheUtils::get(self::CACHE_CONFIG))) {
                        $d = file_exists(self::PATH_OF_CONFIG) ? 
                                file_get_contents(self::PATH_TO_CONFIG) : $this->build(self::PATH_TO_CONFIG, self::PATH_OF_CONFIG);
                        CacheUtils::set(self::CACHE_CONFIG, $d);
                    }                
                $res = $next($req, $res);
                return $res;
            } catch (\Exception $e) {
                throw $e;            
            }
        }

        /**
         * compiling our configuration into a full data
         * @param string $source the directory where configuration are
         * @param string the file we store the final configuration
         * @return string the final configuration data
         */
        private function build(string $source, string $target) : bool {
            if (file_put_contents($target, ($d = json_encode($this->generate($source)))) === false) 
                throw new \RuntimeException("Failed to compile configuration.", self::ERR_BAD_CONFIG);
            return $d;
        }

        /**
         * generate the full configuration set
         * @param string $source the directory where configuration file parts are stored.
         * @return array the full application configuration file
         */
        private function generate(string $source) : array {
            if (file_exsts($source)) {
                $d = dir($source);
                $r = [];
                while (($f = $d->read()) !== false) {
                    if ($f !== "." && $f !== "..") 
                        if (is_dir($t = $source."/".$f)) 
                            $r[$f] = $this->generate($t);
                        else {
                            $n = explode(self::DLT_DOT, $f)[0];
                            $r[$n] = json_decode(file_get_contents($t), true);
                        }
                }
                return $r;
            } else  
                throw new \RuntimeException("Configuration directory $source is missing.", 404);
        }
            
        
        
    }