<?php
    namespace Zimplify\Scaffold\Application\Middlewares;
    use Zimplify\Common\CacheUtils;
    use Slim\Http\Request as request;
    use Slim\Http\Response as response;

    /**
     * the Application is a core instance and have tons of helper functions that we need along the way when using Model, Controller or Views.
     * 
     * @package Zimplify (10)
     * @subpackage Scaffold (02)
     * @category Application (01)
     * @internal Middleware (02)
     * @api: ApplicationOptimizer (03)
     */
    class ApplicationOptimizer {

        const DLT_DASH = "-";
        const DLT_DOT = ".";
        const FILE_OPTIMIZED = "./.optimized.completed";
        const FILE_REOPTIMIZE = "./.reoptimize";
        const FILE_OPTMIZABLES = "./assets";

        /**
         * this is our real procedure
         * @param  ServerRequestInterface $request  PSR7 request
         * @param  ResponseInterface      $response PSR7 response
         * @param  callable               $next     Next middleware
         * @return \Psr\Http\Message\ResponseInterface
         */
        public function __invoke(request $req, response $res, callable $next) {
            try {
                if (file_exists(self::FILE_REOPTIMIZE) || !file_exists(self::FILE_OPTIMIZED)) 
                    $this->synchronize();                
                $res = $next($req, $res);
                return $res;
            } catch (\Exception $e) {
                throw $e;
            }
        }

        /**
         * synchronizing data assets onto cache
         * @param string $source directory containing the items to sync to cache
         * @return void
         */
        protected function synchronize(string $source) : void {
            if (file_exists(self::FILE_OPTMIZABLES)) {
                while (($f = (dir(self::FILE_OPTMIZABLES))->read()) !== false) {
                    if ($f != "." && $f != "..") 
                        if (is_dir($t = $source."/".$f)) 
                            $this->synchronize($t);
                        else {
                            $p = explode(self::DLT_DOT, $f);
                            $a = array_shift($p);
                            $s = array_pop($p);
                            $n = implode(self::DLT_DASH, $p);
                            CacheUtils::set("$s.$a.$n", file_get_contents($t));
                        }
                }
                file_put_contents(self::FILE_OPTIMIZED, "");
            } else 
                throw new \RuntimeException("Unable to find directory for optimization.", 500);
        }
    }