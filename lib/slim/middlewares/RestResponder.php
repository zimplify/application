<?php
    namespace Zimplify\Scaffold\Application\Middlewares;
    use Slim\Http\Request as request;
    use Slim\Http\Response as response;

    /**
     * the Application is a core instance and have tons of helper functions that we need along the way when using Model, Controller or Views.
     * 
     * @package Zimplify (10)
     * @subpackage Scaffold (02)
     * @category Application (01)
     * @internal Middleware (02)
     * @api: RestResponder (01)
     */
    class RestResponder {

        const HTTP_SUCCESS = 200;
        const HDR_TOKEN = "X-Custom-Token";

        /**
         * this is our real procedure
         * @param  ServerRequestInterface $request  PSR7 request
         * @param  ResponseInterface      $response PSR7 response
         * @param  callable               $next     Next middleware
         * @return \Psr\Http\Message\ResponseInterface
         */
        public function __invoke(request $req, response $res, callable $next) {
            try {
                $r = $next($req, $res);
            } catch (\Exception $e) {
                error_log("MW-TRAP> ".$e->getMessage()." [".$e->getCode()."]");
                $d = ["type" => "error", "response" => ["result" => false, "code" => $e->getCode(), "reason" => $e->getMessage()]];
                $res->getBody()->write(json_encode($d));            
            } finally {
                // if we catch something back
                if ($res->getStatusCode() == self::HTTP_SUCCESS) {
                    $d = json_decode($res->getBody(), true);
                    if (array_key_exists("type", $data ?? [])) {
                        $t = $res->getHeader(self::HDR_TOKEN);
                        $r = new response();
                        if ($t) $r = $r->withHeader(self::HDR_TOKEN, $t);
                        $r = $r->withStatus(200);
                        switch ($d["type"]) {
                            case "html":
                                $r = $r->withHeader("Content-Type", "text/html");
                                $r->getBody()->write($d["response"]);
                                break;
                            case "json":
                                $r = $r->withJson($d["response"]);
                                break;
                            case "error":
                                $c = array_key_exists("code", $d["response"]) ? ($d["response"]["code"] > 599 ? 500 : $d["response"]["code"]) : 500;
                                $r = $res->withStatus($c !== 0 ? $c : 500);
                                $r->getBody()->write(json_encode($d["response"]));
                                break;
                            default:
                                $r = $r->setHeader("Content-Type", $d["type"]);
                                $r->getBody()->write($d["data"]);                                
                        }
                    }
                }                
                return $r;
            }
        }
    }