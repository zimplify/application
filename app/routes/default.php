<?php
    // importing all the libraries we need
    use Zimplify\Scaffold\Application\Middlewares\RestResponder;
    use Zimplify\Scaffold\Application\Middlewares\ConfigurationBuilder;
    use Zimplify\Scaffold\Application\Middlewares\ApplicationOptimizer;    
    
    // Entering the application with key routes
    $app->add(new RestResponder());
    $app->add(new ConfigurationBuilder());
    $app->add(new ApplicationOptimizer());

    // insert the main code logic here
    // e.g.;
    //  $app->group("/<some path>", function () {
    //      $this->get("[/]", <SomeController>::class.":<method>");
    //  }

    // DO NOT DELETE, this is fallback for API routing
    $app->any("/[{path:.*}]", function ($req, $res, $args) {                                                              
        $r = ["type" => "error", "response" => ["message" => "Not matching route found", "code" => 404]];        
        $res->getBody()->write(json_encode($r));                                                                 
        return $res;                                                                                             
    });     
    